# encoding utf-8
import pygame
import os
import sys

from space_battle.defaults import (
	WINDOW_HEIGHT,
	WINDOW_WIDTH,
	GAME_FLAG,
	MENU_FLAG,
	OVER_FLAG,
)

from space_battle.ui.game import GameWindow
from space_battle.ui.menu import MenuWindow
from space_battle.ui.over import OverWindow

sys.path.append(os.path.join(sys.path[0], '..'))

if __name__ == '__main__':
	pygame.init()
	window = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
	pygame.display.set_caption('Game')
	clock = pygame.time.Clock()
	flag_game = True

	current_stage = MENU_FLAG
	stages = {
		GAME_FLAG: GameWindow(),
		MENU_FLAG: MenuWindow(),
		OVER_FLAG: OverWindow()
	}

	while flag_game:
		# Количество обновлений логики в секнду (ФПС)
		clock.tick(60)
		# Блок обработки событий
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				flag_game = False
		if not flag_game:
			break

		current = stages.get(current_stage)
		if current:
			result_stage = current.run(window)
			if current_stage == GAME_FLAG and result_stage == OVER_FLAG:
				result = stages.get(result_stage)
				result.score = current.score
			current_stage = result_stage
		else:
			flag_game = False

	pygame.quit()
