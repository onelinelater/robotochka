import pygame
from space_battle.entities.world_object import WorldObject
from space_battle.entities.menu_item import MenuItem


class Menu(WorldObject):
    font_color = (255, 255, 153)

    def __init__(self, selected=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.width = 0
        self.height = 0
        self.x = self.x - self.width / 2
        self.y = self.y - self.height / 2
        self.menu_items = []
        self.max_width = 0
        self.selected = selected

    def add(self, obj):
        if isinstance(obj, MenuItem):
            if not self.selected and not len(self.menu_items):
                self.selected = len(self.menu_items)
            self.menu_items.append(obj)
            obj.color = self.font_color
            self.x = self.x + self.width / 2
            self.y = self.y + self.height / 2

            if obj.width > self.max_width:
                self.max_width = obj.width
                self.width = obj.width
            self.height += obj.height
            self.x = self.x - self.width / 2
            self.y = self.y - self.height / 2
            item_offset_y = 0
            for obj in self.menu_items:
                obj.x = self.x
                obj.y = self.y+item_offset_y
                item_offset_y += obj.height

    def draw(self, window):
        for idx, obj in enumerate(self.menu_items):
            if self.selected == idx:
                pygame.draw.rect(
                    window,
                    obj.highlite_color,
                    (obj.x, obj.y, obj.width, obj.height),
                )
            obj.draw(window)

    def action(self):
        self.menu_items[self.selected].action()
