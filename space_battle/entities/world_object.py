import pygame
from space_battle.defaults import OBJECT_SPEED, DEFAULT_HITBOX_HEIGHT, DEFAULT_HITBOX_WIDTH


class WorldObject(object):
	def __init__(
			self,
			x=10, y=0,
			height=DEFAULT_HITBOX_HEIGHT,
			width=DEFAULT_HITBOX_WIDTH,
			velocity=OBJECT_SPEED,
			color=pygame.Color('White'),
			*args, **kwargs,
	):
		super().__init__(*args, **kwargs)
		self.x = x
		self.y = y
		self.height = height
		self.width = width
		self.velocity = velocity
		self.color = color

	def update(self):
		pass

	def draw(self, window):
		pygame.draw.rect(
			window,
			self.color,
			(self.x, self.y, self.width, self.height),
		)
