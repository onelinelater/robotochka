WINDOW_WIDTH = 1200
WINDOW_HEIGHT = 675

DEFAULT_HITBOX_HEIGHT = 25
DEFAULT_HITBOX_WIDTH = 25
OBJECT_SPEED = 5

BULLET_SPEED = 20  # Скорость полёта пули
BULLET_RADIUS = 5  # Радиус пули
SHOOT_TIME = 1000  # скорость, с которой стреляет противники

BULLET_DAMAGE = 10  # урон пули по-умолчанию
DEFAULT_HP = 10  # количество здоровья по-умолчанию

ENEMY_PARALLEL = 40  # координата y по которой будет перемещаться противник
PLAYER_SPEED = 10  # скорость игрока
PLAYER_SHOOT_TIME = 100  # скорость, с которой стреляет персонаж

HEALTH_BAR_HEIGHT = 5  # высота полоски здоровья
MENU_FONT_SIZE = 50  # размер шрифта

GAME_FLAG = 'play'  # тег для этапа "игра"
MENU_FLAG = 'menu'  # тег для этапа "меню"
OVER_FLAG = 'over'  # тег для этапа "игра окончена"
EXIT_FLAG = 'exit'  # тег для выхода из игры
