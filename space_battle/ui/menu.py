import pygame

from space_battle.ui.window import Window
from space_battle.entities.menu_item import MenuItem
from space_battle.entities.menu import Menu
from space_battle.defaults import MENU_FONT_SIZE, WINDOW_WIDTH, WINDOW_HEIGHT, GAME_FLAG, EXIT_FLAG, MENU_FLAG


class MenuWindow(Window):
    bgcolor = (51, 51, 51)
    last_choice = pygame.time.get_ticks()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.common_font = pygame.font.Font('resources/fonts/joystix monospace.ttf', MENU_FONT_SIZE)
        menu = Menu(x=WINDOW_WIDTH / 2, y=WINDOW_HEIGHT / 2)
        game_start = MenuItem('Start')
        game_start.action = self.play
        menu.add(game_start)
        exit_item = MenuItem('Exit')
        exit_item.action = self.exit
        menu.add(exit_item)
        self.add(menu)

        self.current_stage = MENU_FLAG

    def key_control(self):
        keys = pygame.key.get_pressed()
        if not len(self.game_objects):
            return
        if self.last_choice and keys[self.last_choice]:
            return

        self.last_choice = None
        menu = self.game_objects[0]

        if keys[pygame.K_RETURN]:
            menu.action()

        if keys[pygame.K_UP]:
            menu.selected -= 1
            self.last_choice = pygame.K_UP
        if keys[pygame.K_DOWN]:
            menu.selected += 1
            self.last_choice = pygame.K_DOWN
        if menu.selected > len(menu.menu_items) - 1:
            menu.selected = 0
        if menu.selected < 0:
            menu.selected = len(menu.menu_items) - 1

    def prepare_window(self, window):
        window.fill(self.bgcolor)

    def play(self):
        self.current_stage = GAME_FLAG

    def exit(self):
        self.current_stage = EXIT_FLAG
