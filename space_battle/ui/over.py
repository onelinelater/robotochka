import pygame

from space_battle.ui.window import Window
from space_battle.defaults import MENU_FONT_SIZE, WINDOW_WIDTH, WINDOW_HEIGHT, MENU_FLAG, OVER_FLAG


class OverWindow(Window):
    bgcolor = (51, 51, 51)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.text = 'Game Over!'
        self.score = 0
        self.common_font = pygame.font.Font('resources/fonts/joystix monospace.ttf', MENU_FONT_SIZE)
        self.current_stage = OVER_FLAG

    def key_control(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_RETURN]:
            self.menu()

    def prepare_window(self, window):
        window.fill(self.bgcolor)
        text = self.text
        width1, height1 = self.common_font.size(text)
        width2, height2 = self.common_font.size(text)
        common_height = height1 + height2
        window.blit(
            self.common_font.render(
                self.text,
                True,
                pygame.Color("Green"),
            ), (
                WINDOW_WIDTH / 2 - width1 / 2,
                WINDOW_HEIGHT / 2 - common_height/2,
            )
        )
        window.blit(
            self.common_font.render(
                f'Score is: {self.score}',
                True,
                pygame.Color("Green"),
            ), (
                WINDOW_WIDTH / 2 - width2 / 2,
                WINDOW_HEIGHT / 2 + height2 / 2,
            )
        )

    def menu(self):
        self.current_stage = MENU_FLAG
