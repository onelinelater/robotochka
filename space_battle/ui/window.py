import pygame
from space_battle.entities.world_object import WorldObject


class Window(object):
	def __init__(self, *args, **kwargs):
		self.game_objects = []
		self.current_stage = None

	def add(self, obj):
		if isinstance(obj, WorldObject):
			self.game_objects.append(obj)

	def remove(self, obj):
		self.game_objects.remove(obj)

	def prepare_window(self, window):
		pass

	def key_control(self):
		pass

	def run(self, window):
		self.prepare_window(window)
		for game_object in self.game_objects:
			game_object.update()
		self.key_control()
		for game_object in self.game_objects:
			game_object.draw(window)
		pygame.display.update()

		return self.current_stage
