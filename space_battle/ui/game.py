import pygame
from space_battle.ui.window import Window
from space_battle.defaults import (
	WINDOW_HEIGHT,
	WINDOW_WIDTH,
	ENEMY_PARALLEL,
	MENU_FONT_SIZE,
	PLAYER_SHOOT_TIME,
	PLAYER_SPEED,
	GAME_FLAG,
	OVER_FLAG,
)
from space_battle.entities.player import Player
from space_battle.entities.enemy import Enemy


class GameWindow(Window):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.score = 0
		self.common_font = pygame.font.Font('resources/fonts/joystix monospace.ttf', MENU_FONT_SIZE)
		player_img = pygame.image.load('resources//images/jet1.png')
		self.player = Player(
			player_img,
			x=(WINDOW_WIDTH - player_img.get_width()) / 2,
			y=WINDOW_HEIGHT - player_img.get_height(),
			shoot_time=PLAYER_SHOOT_TIME,
			velocity=PLAYER_SPEED,
		)
		self.add(self.player)
		enemy_img = pygame.image.load('resources//images/hotpng.com (4).png')
		self.enemy = Enemy(
			enemy_img,
			y=ENEMY_PARALLEL,
		)
		self.add(self.enemy)
		self.player.holder = self.game_objects
		self.enemy.holder = self.game_objects

		self.player.target = self.enemy
		self.enemy.target = self.player

		self.current_stage = GAME_FLAG

	def key_control(self):
		# Блок управления персонажем
		keys = pygame.key.get_pressed()
		player = self.player
		enemy = self.enemy

		if keys[pygame.K_LEFT] and player.x > player.velocity:
			player.x -= player.velocity
		if keys[pygame.K_RIGHT] and player.x < (WINDOW_WIDTH - player.velocity - player.width):
			player.x += player.velocity
		if keys[pygame.K_UP] and player.y > player.velocity + ENEMY_PARALLEL + enemy.height:
			player.y -= player.velocity
		if keys[pygame.K_DOWN] and player.y < (WINDOW_HEIGHT - player.velocity - player.height):
			player.y += player.velocity
		if keys[pygame.K_SPACE]:
			player.shoot()

	def prepare_window(self, window):
		window.fill((51, 51, 51))
		window.blit(
			self.common_font.render(
				f'Score is: {self.score}',
				True,
				pygame.Color("Green"),
			), (0, 0,)
		)

	def over(self):
		self.current_stage = OVER_FLAG

	def run(self, window):
		super().run(window)
		if self.enemy.dead:
			self.enemy.respawn()
			self.score += 100
		if self.player.dead:
			self.over()

		return self.current_stage
