**ЗАДАНИЕ НА УРОК 8**, где появится меню, окно "конец игры" и взаимодействие между ними
======

Ребята! Не пропускайте пункты задания. Например, если вы не импортировали константу и потом
использовали её, ничего работать не будет. Если в тексте задания что-то показалось вам не понятным или у вас 
есть предложения, как улучшить процесс обучения, вы всегда можете отправить комментарий на мой электронный ящик 
`onelinelater@gmail.com`. Я постараюсь учесть ваши пожелания.

Добавляем константы в файл defaults.py
------

Добавим теги для этапов игры:
```
GAME_FLAG = 'play'  # тег для этапа "игра"
MENU_FLAG = 'menu'  # тег для этапа "меню"
OVER_FLAG = 'over'  # тег для этапа "игра окончена"
EXIT_FLAG = 'exit'  # тег для выхода из игры
```

Добавляем меню и элемент меню
------

Прежде всего займёмся этапом "меню". Для того, чтобы вывести само меню и его элементы, нужно
создать соответствующие классы. Создайте файлы `menu.py` и `menu_item.py` в папке `entities`.
Перенесите код ниже в эти файлы:

***menu_item.py***

```
from space_battle.entities.world_object import WorldObject
import pygame
from space_battle.defaults import MENU_FONT_SIZE


def default_action():
	pass


class MenuItem(WorldObject):
	highlite_color = (153, 102, 255)
	action = default_action

	def __init__(self, text, *args, **kwargs):
		super(MenuItem, self).__init__(*args, **kwargs)
		self.text = text
		self.font = pygame.font.Font('resources/fonts/joystix monospace.ttf', MENU_FONT_SIZE)
		width, height = self.font.size(text)
		self.width = width
		self.height = height

	def draw(self, window):
		window.blit(self.font.render(self.text, True, self.color), (self.x, self.y))

```

***menu.py***

```
import pygame
from space_battle.entities.world_object import WorldObject
from space_battle.entities.menu_item import MenuItem


class Menu(WorldObject):
    font_color = (255, 255, 153)

    def __init__(self, selected=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.width = 0
        self.height = 0
        self.x = self.x - self.width / 2
        self.y = self.y - self.height / 2
        self.menu_items = []
        self.max_width = 0
        self.selected = selected

    def add(self, obj):
        if isinstance(obj, MenuItem):
            if not self.selected and not len(self.menu_items):
                self.selected = len(self.menu_items)
            self.menu_items.append(obj)
            obj.color = self.font_color
            self.x = self.x + self.width / 2
            self.y = self.y + self.height / 2

            if obj.width > self.max_width:
                self.max_width = obj.width
                self.width = obj.width
            self.height += obj.height
            self.x = self.x - self.width / 2
            self.y = self.y - self.height / 2
            item_offset_y = 0
            for obj in self.menu_items:
                obj.x = self.x
                obj.y = self.y+item_offset_y
                item_offset_y += obj.height

    def draw(self, window):
        for idx, obj in enumerate(self.menu_items):
            if self.selected == idx:
                pygame.draw.rect(
                    window,
                    obj.highlite_color,
                    (obj.x, obj.y, obj.width, obj.height),
                )
            obj.draw(window)

    def action(self):
        self.menu_items[self.selected].action()

```

Разберитесь, что в них происходит. Класс MenuItem содержит поля для подсветки, цвета шрифта, шрифте
и функционал для выполнения комманд. Класс Menu реализует функционал подсветки и выбора строк меню
и выравнивания пунктов меню по центру.

Поправим класс Window в файле window.py папки ui
------

Добавьте поле `self.current_stage = None` в конструктор и замените `return True` 
на `return self.current_stage`

Поправим класс GameWindow в файле game.py папки ui
------

*Импортируйте* константы `GAME_FLAG, OVER_FLAG,` и добавьте 
поле `self.current_stage = GAME_FLAG` в конструктор.

Добавьте метод `over`

```
	def over(self):
		self.current_stage = OVER_FLAG
```

В методе `run` замените 

```
if self.player.dead:
    return False
```

на 

```
if self.player.dead:
    return self.over(
```

а код в конце метода

```
return True
```

на 

```
return self.current_stage
```

Добавим класс MenuWindow
------

Итак, время реализовать этап "Меню". Этот этап должен свести всё воедино.
Листинг этапа представлен ниже. Внимательно прочитайте его и реализуйте у себя.

```
import pygame

from space_battle.ui.window import Window
from space_battle.entities.menu_item import MenuItem
from space_battle.entities.menu import Menu
from space_battle.defaults import MENU_FONT_SIZE, WINDOW_WIDTH, WINDOW_HEIGHT, GAME_FLAG, EXIT_FLAG, MENU_FLAG


class MenuWindow(Window):
    bgcolor = (51, 51, 51)
    last_choice = pygame.time.get_ticks()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.common_font = pygame.font.Font('resources/fonts/joystix monospace.ttf', MENU_FONT_SIZE)
        menu = Menu(x=WINDOW_WIDTH / 2, y=WINDOW_HEIGHT / 2)
        game_start = MenuItem('Start')
        game_start.action = self.play
        menu.add(game_start)
        exit_item = MenuItem('Exit')
        exit_item.action = self.exit
        menu.add(exit_item)
        self.add(menu)

        self.current_stage = MENU_FLAG

    def key_control(self):
        keys = pygame.key.get_pressed()
        if not len(self.game_objects):
            return
        if self.last_choice and keys[self.last_choice]:
            return

        self.last_choice = None
        menu = self.game_objects[0]

        if keys[pygame.K_RETURN]:
            menu.action()

        if keys[pygame.K_UP]:
            menu.selected -= 1
            self.last_choice = pygame.K_UP
        if keys[pygame.K_DOWN]:
            menu.selected += 1
            self.last_choice = pygame.K_DOWN
        if menu.selected > len(menu.menu_items) - 1:
            menu.selected = 0
        if menu.selected < 0:
            menu.selected = len(menu.menu_items) - 1

    def prepare_window(self, window):
        window.fill(self.bgcolor)

    def play(self):
        self.current_stage = GAME_FLAG

    def exit(self):
        self.current_stage = EXIT_FLAG

```

Добавим класс OverWindow
------

Этот класс позволяет вывести на экран текст "Игра окночена" и счёт по центру экрана, а так же
позволяет перейти обратно в меню при нажатии клавиши *ENTER*.

```
import pygame

from space_battle.ui.window import Window
from space_battle.defaults import MENU_FONT_SIZE, WINDOW_WIDTH, WINDOW_HEIGHT, MENU_FLAG, OVER_FLAG


class OverWindow(Window):
    bgcolor = (51, 51, 51)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.text = 'Game Over!'
        self.score = 0
        self.common_font = pygame.font.Font('resources/fonts/joystix monospace.ttf', MENU_FONT_SIZE)
        self.current_stage = OVER_FLAG

    def key_control(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_RETURN]:
            self.menu()

    def prepare_window(self, window):
        window.fill(self.bgcolor)
        text = self.text
        width1, height1 = self.common_font.size(text)
        width2, height2 = self.common_font.size(text)
        common_height = height1 + height2
        window.blit(
            self.common_font.render(
                self.text,
                True,
                pygame.Color("Green"),
            ), (
                WINDOW_WIDTH / 2 - width1 / 2,
                WINDOW_HEIGHT / 2 - common_height/2,
            )
        )
        window.blit(
            self.common_font.render(
                f'Score is: {self.score}',
                True,
                pygame.Color("Green"),
            ), (
                WINDOW_WIDTH / 2 - width2 / 2,
                WINDOW_HEIGHT / 2 + height2 / 2,
            )
        )

    def menu(self):
        self.current_stage = MENU_FLAG

```

Совместим всё в файле main.py
------

Импортируйет константы `GAME_FLAG, MENU_FLAG, OVER_FLAG,` и созданные ранее классы

```
from space_battle.ui.game import GameWindow
from space_battle.ui.menu import MenuWindow
from space_battle.ui.over import OverWindow
```

затем, давайте реализуем словарь со всеми состояниями игры перед основным игровым циклом:

```
current_stage = MENU_FLAG
stages = {
    GAME_FLAG: GameWindow(),
    MENU_FLAG: MenuWindow(),
    OVER_FLAG: OverWindow()
}
```

наконец, добавим логики переходов в главный цикл:

```
current = stages.get(current_stage)
if current:
    result_stage = current.run(window)
    if current_stage == GAME_FLAG and result_stage == OVER_FLAG:
        result = stages.get(result_stage)
        result.score = current.score
    current_stage = result_stage
else:
    flag_game = False
```

Готово! Наконец, после стольки занятий, игра полностью реализована! Вы можете собой гордиться!
