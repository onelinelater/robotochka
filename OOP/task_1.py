import random


class Student(object):
	def __init__(self, fio, group_number, marks):
		self.fio = fio
		self.group_number = group_number
		self.marks = marks
	
	def middle(self):
		return sum(self.marks) / len(self.marks)

	def is_good_one(self):
		return all((item >= 4 for item in self.marks))

	def __str__(self):
		return '{} группа {} средняя оценка {}'.format(self.fio, self.group_number, self.middle())


students = [
	Student(
			"БАТЯ!!! О.Х.",
			1,
			[
				random.randint(1, 5)
				for i in range(random.randint(4, 10))
			]
		),
	Student(
			"Ник М.Е.",
			1,
			[
				random.randint(1, 5)
				for i in range(random.randint(4, 10))
			]
		),
	Student(
		"Марков И.Е.",
		1,
		[
			random.randint(1, 5)
			for i in range(random.randint(4, 10))
		]
	),
	Student(
		"Леонов И.Н.",
		2,
		[
			random.randint(1, 5)
			for i in range(random.randint(4, 10))
		]
	),
	Student(
		"Саша В.Е.",
		3,
		[
			random.randint(1, 5)
			for i in range(random.randint(4, 10))
		]
	),
	Student(
		"Ваня А.Е.",
		4,
		[
			random.randint(1, 5)
			for i in range(random.randint(4, 10))
		]
	),
	Student(
		"Петя Н.Е.",
		5,
		[
			random.randint(3, 5)
			for i in range(random.randint(4, 10))
		]
	),
	Student(
		"Супер Н.Л.",
		6,
		[4, 5]
	),
	Student(
		"Мега С.Л.",
		6,
		[4, 5]
	),
	Student(
		"Буба П.А.",
		6,
		[4, 5]
	),
]

students = sorted(students, key=lambda x: x.middle())

print("Все")
for student in students:
	print(student)
print("Хорошисты")

for student in students:
	if student.is_good_one():
		print(student)

input()