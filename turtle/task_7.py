import turtle

def square(a, b):
	turtle.begin_fill()
	for i in range(4):
		if i % 2:
			turtle.forward(a)
		else:
			turtle.forward(b)
		turtle.left(90)
	turtle.end_fill()

step = 8
height = 100
width = step / 2  
turtle.colormode(255)
turtle.speed(200)
turtle.backward(200)

colors = (
	[(255,i,0,) for i in range(0,255,step)],
	[(255 - i,255,0,) for i in range(0,255,step)],
	[(0,255,i,) for i in range(0,255,step)],
	[(0,255-i,255,) for i in range(0,255,step)],
	[(i,0,255,) for i in range(0,255,step)],
	[(255,0,255-i,) for i in range(0,255,step)],
)

for color in colors:
	for i in color:
		turtle.color(i)
		square(height, width)
		turtle.up()
		turtle.forward(width)
		turtle.down()

turtle.exitonclick()
