import turtle       
turtle.reset()
turtle.down()

turtle.color('red')
turtle.begin_fill()
turtle.left(45)
turtle.circle(20)
turtle.end_fill()
turtle.up()
turtle.forward(100)

turtle.exitonclick()
