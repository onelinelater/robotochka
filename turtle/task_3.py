import turtle
turtle.reset()
turtle.color('purple')
a = 100
turtle.left(90)
for i in range(0, 5):
	turtle.forward(a)
	turtle.right(90)
	if i % 2 == 0:
		a -= 20
turtle.exitonclick()