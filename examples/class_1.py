import math

class Decimal(object): 
	numerator = 1
	denomenator = 2

	def __init__(self, numerator, denomenator):
		self.numerator = numerator 
		self.denomenator  = denomenator
		self.to_shorten()

	def __str__(self):
		return '{}/{}'.format(self.numerator, self.denomenator)

	def __mul__(self, other):
		return Decimal(
			self.numerator*other.numerator,
			self.denomenator*other.denomenator,
		)

	def __truediv__(self, other):
		return Decimal(
			self.numerator*other.denomenator,
			self.denomenator*other.numerator,
		)

	def to_shorten(self):
		common = math.gcd(self.numerator, self.denomenator)
		self.numerator //= common
		self.denomenator //= common

	def to_denomenator(self, denomenator):
		new_
		self.numerator *= denomenator // self.denomenator
		self.denomenator = denomenator


example_decimal_1 = Decimal(1, 2)
example_decimal_1.to_denomenator(4)
print(example_decimal_1)
input()