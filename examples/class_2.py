class Worker(object):
	def __init__(self, prof):
		self.prof = prof

	def _protected(self):
		print("Тайна")
		self.__private()

	def __private(self):
		print("Почти тайна")
	
	def say_hello(self):
		print("Привет! Я {}!".format(self.prof))

evgraf = Worker(9)
evgraf._protected()

class Person(Worker):
	def __init__(self, age, prof):
		super().__init__(prof)
		self.age = age

	def say_hello(self):
		super().say_hello()
		print("Мне {} лет.".format(self.age))

	def __str__(self):
		return "Человек возрастом {} лет".format(self.age)

class Proffesional(Worker):
	def __init__(self, stage, prof):
		super().__init__(prof)
		self.stage = stage

	def say_hello(self):
		super().say_hello()
		print("Мой стаж {} лет.".format(self.stage))

	def __str__(self):
		return "Работник стажем {} лет".format(self.stage)


petya = Proffesional(10, "программист")
vasya = Person(20, "столяр")

input()