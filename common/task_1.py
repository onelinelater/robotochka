# Написать программу arithmetic, принимающую 3 аргумента:
# два числа и операция, которая должна быть произведена над ними.
# Если третий аргумент +, сложить их; если —, то вычесть; * — умножить; / — разделить (первое на второе).
# В остальных случаях вернуть строку "Неизвестная операция".


operations = {
	'+': lambda a, b: a+b,
	'-': lambda a, b: a-b,
	'*': lambda a, b: a*b,
	'/': lambda a, b: a/b,
}

first_number = int(input('Введите первое число: '))
second_number = int(input('Введите второе число: '))
operation = input('Введите операцию: ')
if operation in operations:
	print(operations[operation](first_number, second_number ))
else:
	print('Неизвестная операция')
input()
